package org.delta.core.tests;


import org.delta.core.testing.dataprovider.DataSource;
import org.delta.core.testing.dataprovider.DataSourceTable;
import org.delta.core.testing.dataprovider.ProviderFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class DataSourceTest {

    private static final String dsLocation = "samples/UIData.xlsx" ;
    static DataSource dataSource;

    @BeforeClass
    public static void beforeClass(){
        dataSource = ProviderFactory.dataSource(dsLocation);
        Assert.assertNotNull(dataSource);
        Assert.assertNotNull(dataSource.tables);
        Assert.assertFalse(dataSource.tables.isEmpty());

    }

    @Test
    public void testSimpleData() throws Exception{
        dataSource.tables.forEach((key, value) -> {
            Assert.assertNotNull(value.columns());
            Assert.assertFalse( value.columns().isEmpty() );
            Assert.assertTrue( value.length() > 0 );
            for ( int i=0; i < value.length(); i++ ){
                Map<?,?> m = value.tuple(i);
                Assert.assertNotNull( m );
                Assert.assertFalse( m.isEmpty() );
            }
        });
    }

    public static void testSkewRow(DataSourceTable table , int rowInx, int expSize){
        String[] row = table.row(rowInx);
        Assert.assertNotNull(row);
        Assert.assertEquals(expSize, row.length);
    }
    @Test
    public void testSkewedData() throws Exception{
        final String arrayDemoTable = "ArrayData" ;
        DataSourceTable table = dataSource.tables.get(arrayDemoTable);
        Assert.assertNotNull(table);
        testSkewRow(table, 0, 3);
        testSkewRow(table, 1, 5);
        testSkewRow(table, 2, 2);
    }


    public static void testArray(DataSourceTable table , int rowInx, int expSize){
        Map<String,Object> m = table.relationalTuple(rowInx);
        Assert.assertNotNull(m);
        Assert.assertFalse( m.isEmpty() );
        final String keyName = "Array" ;
        Object o = m.get(keyName);
        Assert.assertNotNull(o);
        Assert.assertTrue( o instanceof List);
        List<?> l = (List<?>)o;
        Assert.assertEquals( expSize, l.size());

    }
    @Test
    public void testArray(){
        final String arrayDemoTable = "ArrayDemo" ;
        DataSourceTable table = dataSource.tables.get(arrayDemoTable);
        Assert.assertNotNull(table);
        Assert.assertTrue(table.length() > 0 );
        testArray(table, 1, 3);
        testArray(table, 2, 5);
        testArray(table, 3, 2);
    }

}
