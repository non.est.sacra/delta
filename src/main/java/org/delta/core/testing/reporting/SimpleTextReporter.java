
package org.delta.core.testing.reporting;

import org.delta.core.testing.Utils;
import org.delta.core.testing.TestSuiteRunner;
import zoomba.lang.core.interpreter.ZAssertion;
import zoomba.lang.core.types.ZNumber;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of a @{Reporter} - simple text one
 */
public class SimpleTextReporter implements Reporter {

    /**
     * Gets a reporter
     * @param mode what sort of sync we are using? See @{SyncMode}
     * @param location where we want it to go
     * @return a reporter initialized properly
     */
    public static SimpleTextReporter reporter(Number mode, String location){
        SimpleTextReporter reporter = new SimpleTextReporter();
        ArrayList<String> list = new ArrayList<>();
        list.add(String.valueOf(mode.byteValue()));
        reporter.init(list);
        reporter.location(location);
        return reporter ;
    }


    /**
     * The Sync type
     */
    public static class SyncMode {
        // objective is to enable console and file logging together ...
        private byte mode = 3;

        public SyncMode(String s){
           mode = ZNumber.integer(s,3).byteValue() ;
        }

        /**
         * Console type - stdout and stderr
         */
        public void setConsole(){
            mode = (byte) (mode | 1);
        }
        /**
         * Just ignores the logging
         */
        public void setNull(){
            mode = 0;
        }
        /**
         * File type
         */
        public void setFile(){
            mode = (byte) (mode | 2);
        }

        public boolean isNull(){
            return mode == 0;
        }

        public boolean isConsole(){
            return ( mode & 1 ) != 0;
        }

        public boolean isFile(){
            return ( mode & 2 ) != 0;
        }

    }

    public  interface  FormattedAppender{
        void printf(String format, Object... args) ;
        void append(PrintStream ps);
    }

    protected FormattedAppender printStream = new FormattedAppender() {
        private final List<PrintStream> inner = new ArrayList<>();
        @Override
        public void printf(String format, Object... args) {
            inner.forEach(ps -> {
                ps.printf(format,args);
                ps.flush();
            });
        }

        @Override
        public void append(PrintStream ps) {
            inner.add(ps);
        }
    };

    protected String location;

    protected String fileName = "TextReport.txt" ;

    SyncMode type = new SyncMode("3");

    @Override
    public String location() {
        return location;
    }

    @Override
    public void init(List<String> args) {
        if ( args.size() == 0 ){
            return;
        }
        type = new SyncMode( args.get(0));
        if ( args.size() > 1 ){
            fileName = args.get(2);
        }
    }

    @Override
    public void location(String location) {
        this.location = "";
        if ( type.isConsole() ){
            printStream.append(System.out);
        }

        if (type.isFile() ) {
            try {
                this.location = location +"/" +  name();
                PrintStream ps = new PrintStream(this.location);
                printStream.append(ps);
            } catch (Exception e) {
                this.location = "";
            }
        }
    }

    @Override
    public String name() {
        return fileName ;
    }

    @Override
    public void onAssertion(ZAssertion.AssertionEvent assertionEvent) {
        printStream.printf("%s|%s:%d|@ %s\n", Utils.ts(), dsTable,row,assertionEvent);
    }

    String dsTable;
    int row;

    @Override
    public void onTestRunEvent(TestSuiteRunner.TestRunEvent testRunEvent) {
        if ( type.isNull() ){
            return;
        }
        switch (testRunEvent.type){
            case BEFORE_FEATURE:
            case AFTER_FEATURE:
                printStream.printf("%s|%s|%s\n", Utils.ts(), testRunEvent.feature, testRunEvent.type);
                break;

            case IGNORE_TEST:
                dsTable = testRunEvent.table.name() ;
                row = testRunEvent.row ;
                printStream.printf("%s|%s|%s:%d|%s\n", Utils.ts(),
                        testRunEvent.feature, dsTable, row , testRunEvent.type);
                break;

            case BEFORE_TEST:
                dsTable = testRunEvent.table.name() ;
                row = testRunEvent.row ;
            case ABORT_TEST:
                printStream.printf("%s|%s|%s:%d|%s\n", Utils.ts(),
                        testRunEvent.feature, dsTable, row , testRunEvent.type);
                break;

            case OK_TEST:
                printStream.printf("%s|%s|%s:%d|%s >o> %s \n",Utils.ts(),
                        testRunEvent.feature,
                        dsTable,row , testRunEvent.type,testRunEvent.runObject );
                break;
            case ERROR_TEST:
                printStream.printf("%s|%s|%s:%d|%s >e> %s \n",Utils.ts(),
                        testRunEvent.feature,
                        dsTable, row , testRunEvent.type,testRunEvent.error );
                break;
        }
    }
}
