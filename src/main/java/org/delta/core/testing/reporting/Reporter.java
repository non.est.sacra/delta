package org.delta.core.testing.reporting;

import org.delta.core.testing.TestSuiteRunner;
import zoomba.lang.core.interpreter.ZAssertion;
import java.util.List;

/**
 * A generic reporter infra
 */
public interface Reporter extends TestSuiteRunner.TestRunEventListener,
        ZAssertion.AssertionEventListener {

    /**
     * Arbitrary args to pass for initializing the reporter
     * @param args a list of arguments, all string
     */
    void init(List<String> args);

    /**
     * Where we want the log to be created
     * @param location the path to the log
     */
    void location(String location);

    /**
     * Gets back the current path of the log
     * @return location of the log
     */
    String location();

    /**
     * What name this reporter has
     * @return name of the reporter
     */
    String name();
}
