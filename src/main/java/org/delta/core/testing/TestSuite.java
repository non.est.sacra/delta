
package org.delta.core.testing;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.security.AnyTypePermission;
import org.delta.core.testing.ui.XSelenium;
import org.openqa.selenium.InvalidArgumentException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

/**
 */
@XStreamAlias("testSuite")
public class TestSuite {

    public String location ;

    @XStreamAsAttribute
    public String version;

    @XStreamAlias("source")
    public static class DataSource{

        public static final String TEST_ID = "#testId#" ;

        public static final String TEST_ENABLE = "#enable#" ;

        @XStreamAsAttribute
        public String name;

        @XStreamAsAttribute
        public String location;

        @XStreamAsAttribute
        public String testIdColumn;

        @XStreamAsAttribute
        public String testEnableColumn;

        public DataSource(){
            name="";
            location="";
            testIdColumn = TEST_ID ;
            testEnableColumn = TEST_ENABLE ;
        }
    }

    public static class ObjectInit{

        @XStreamAsAttribute
        public String name;

        @XStreamAsAttribute
        public String type;

        @XStreamImplicit(itemFieldName = "param")
        public ArrayList<String> params;

        public ObjectInit(){
            name="";
            type="";
            params = new ArrayList<>();
        }
    }

    @XStreamAlias("reporter")
    public static class Reporter extends ObjectInit{}

    public static class Feature {

        @XStreamAsAttribute
        public String name ;

        @XStreamAsAttribute
        public String base ;

        @XStreamAsAttribute
        public String method ;

        @XStreamAsAttribute
        public String ds ;

        @XStreamAsAttribute
        public String table ;

        @XStreamAsAttribute
        public boolean randomize;

        @XStreamAsAttribute
        public String owner ;

        @XStreamAsAttribute
        public boolean enabled ;

        @XStreamAsAttribute
        public String script ;

        @XStreamAsAttribute
        public String beforeScript ;

        @XStreamAsAttribute
        public String afterScript ;

        @XStreamAsAttribute
        public int timeout ;

        public Feature(){
            name = "" ;
            base = "" ;
            method = "" ;
            ds = "" ;
            table = "" ;
            owner = "" ;
            randomize = false ;
            enabled = true ;
            script = "";
            beforeScript = "" ;
            afterScript = "" ;
            timeout = 10000;
        }
    }

    public static class Application{

        @XStreamAsAttribute
        public String name;

        @XStreamAsAttribute
        public String build;

        @XStreamAsAttribute
        public String scriptDir;

        @XStreamAsAttribute
        public String logs;


        @XStreamImplicit(itemFieldName = "feature")
        public ArrayList<Feature> features;

        public Application(){
            features = new ArrayList<>();
            name= "";
            build = "" ;
            scriptDir ="" ;
            logs = "" ;
        }
    }

    public ArrayList<DataSource> dataSources;

    public ArrayList<Reporter> reporters;

    public TestSuite(){
        location = "" ;
        version = "0.1";
        dataSources = new ArrayList<>();
        reporters = new ArrayList<>();
    }


    public static class EnumConverter implements Converter {

        public Class<? extends Enum<?>> enumClass;

        public EnumConverter( Class<? extends Enum<?>> enumClass ){
            this.enumClass = enumClass;
        }

        @Override
        public void marshal(Object o, HierarchicalStreamWriter hierarchicalStreamWriter, MarshallingContext marshallingContext) {
            // we do not use it...
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader hierarchicalStreamReader, UnmarshallingContext unmarshallingContext) {
            String val = hierarchicalStreamReader.getValue();
            Optional<? extends Enum<?>> opt = Arrays.stream(enumClass.getEnumConstants()).filter( x -> x.name().equals(val)).findFirst();
            if ( opt.isPresent() ){
                return opt.get();
            }
            throw new InvalidArgumentException( "Invalid enum value : " + val );
        }

        @Override
        public boolean canConvert(Class aClass) {
            // do exact match
            return enumClass.equals(aClass);
        }
    }

    protected static <T extends TestSuite> T loadFrom(
            Class c , String xmlFile, Map<String,String> variables)
            throws Exception{

        if ( !TestSuite.class.isAssignableFrom(c)){
            throw new Exception("Sorry pal, [" + c + "] is not a TestSuite!" );
        }
        XStream xStream = new XStream(new PureJavaReflectionProvider());
        xStream.alias("testSuite", c);
        xStream.autodetectAnnotations(true);
        String xml = Utils.readToEnd(xmlFile);
        // now replace all variables
        xml  = Utils.substituteVariableInXml(xml,variables);
        // now replace the paths
        String location = new File(xmlFile).getCanonicalPath();
        location = location.replace('\\','/');
        String dir = location.substring(0, location.lastIndexOf("/"));
        // do the magical relocation here
        xml = Utils.relocatePathInXml(dir, xml);
        // fix for :: https://stackoverflow.com/questions/30812293/com-thoughtworks-xstream-security-forbiddenclassexception
        xStream.addPermission(AnyTypePermission.ANY);
        xStream.registerConverter( new EnumConverter(XSelenium.BrowserType.class) );
        T obj = (T)xStream.fromXML(xml);
        obj.location = location ;
        return  obj;
    }
}
