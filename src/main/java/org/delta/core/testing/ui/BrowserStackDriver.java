package org.delta.core.testing.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import zoomba.lang.core.types.ZTypes;
import zoomba.lang.core.types.ZXml;
import java.io.FileInputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * This class is the one connects to:
 * href : https://www.browserstack.com/automate/java#setting-os-and-browser
 * The idea is to generate a remote webdriver
 * Which sets the capabilities well
 */
public class BrowserStackDriver extends XWebDriver {

    public static final String BS_LOCAL_ID = "BS_LOCAL_ID" ;

    public static class BrowserStackConfiguration extends DesiredCapabilities{

        public final String user;

        public final String key;

        public final boolean local;

        public BrowserStackConfiguration(Map<String,?> raw) {
            super(raw);
            user = String.valueOf( getCapability("user") );
            key = String.valueOf( getCapability("key") );
            local = ( getCapability("browserstack.local") != null );
            if ( local ){
                Map<String, String> env = System.getenv();
                if ( env.containsKey(BS_LOCAL_ID) ){
                    String localId = env.get(BS_LOCAL_ID);
                    setCapability("browserstack.localIdentifier", localId );
                }
            }
        }

        public static BrowserStackConfiguration loadFromXml(String xmlFile) throws Exception {
            ZXml xmlMap = ZXml.xml(xmlFile,true) ;
            List<ZXml.ZXmlElement> elements = xmlMap.root.children();
            Map<String,String> map = new HashMap<>();
            for (ZXml.ZXmlElement element : elements ){
                String name = element.node.getNodeName() ;
                String value = element.node.getTextContent() ;
                map.put(name, value);
            }
            return fromMap(map);
        }

        public static BrowserStackConfiguration fromMap(Map map) {
            return new BrowserStackConfiguration(map);
        }

        public static BrowserStackConfiguration loadFromJSON(String jsonFile) throws Exception {
            Map map = (Map) ZTypes.json(jsonFile,true);
            BrowserStackConfiguration configuration = fromMap(map);
            return configuration;
        }

        public static BrowserStackConfiguration loadFromText(String propertyFile) throws Exception {
            Properties properties = new Properties();
            properties.load(new FileInputStream(propertyFile));
            BrowserStackConfiguration configuration = fromMap(properties);
            return configuration;
        }
    }

    public static BrowserStackDriver createDriver(String file){
        BrowserStackConfiguration config ;
        // create driver here...
        String s_file = file.toLowerCase();
        try {
            if (s_file.endsWith(".xml")) {
                config = BrowserStackConfiguration.loadFromXml(file);
            } else if (s_file.endsWith(".json")) {
                config = BrowserStackConfiguration.loadFromJSON(file);
            }else{
                config = BrowserStackConfiguration.loadFromText(file);
            }

        } catch (Throwable t) {
            throw new Error("Issue Creating a BrowserStack remote driver from !", t);
        }

        String url = String.format("http://%s:%s@hub.browserstack.com/wd/hub",
                config.user, config.key);

        try {
            RemoteWebDriver driver = new RemoteWebDriver(new URL(url), config);
            return new BrowserStackDriver(driver);
        }catch (Exception e){
            throw new Error(e);
        }
    }

    public BrowserStackDriver(WebDriver driver) {
        super(driver);
    }
}
