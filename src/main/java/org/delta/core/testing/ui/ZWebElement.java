package org.delta.core.testing.ui;

import org.openqa.selenium.*;
import zoomba.lang.core.collections.ZList;

import java.util.List;

/**
 */
public class ZWebElement implements WebElement {

    public final WebElement inner;

    public Object get(String name){
        switch (name){
            case "text" :
                return getText();
            default:
                return getAttribute(name);
        }
    }

    public List elements(String locator){
        By by = XSelenium.getByFromLocator(locator);
        List<WebElement> el = findElements(by);
        ZList l = new ZList();
        for ( WebElement we : el){
            l.add( new ZWebElement(we) );
        }
        return l;
    }

    public WebElement element(String locator){
        By by = XSelenium.getByFromLocator(locator);
        WebElement we = findElement(by);
        return new ZWebElement(we);
    }

    public ZWebElement(WebElement element){
        inner = element;
    }

    @Override
    public void click() {
        inner.click();
    }

    @Override
    public void submit() {
        inner.submit();
    }

    @Override
    public void sendKeys(CharSequence... charSequences) {
        inner.sendKeys(charSequences);
    }

    @Override
    public void clear() {
        inner.clear();
    }

    @Override
    public String getTagName() {
        return inner.getTagName();
    }

    @Override
    public String getAttribute(String s) {
        return inner.getAttribute(s);
    }

    @Override
    public boolean isSelected() {
        return inner.isSelected();
    }

    @Override
    public boolean isEnabled() {
        return inner.isEnabled();
    }

    @Override
    public String getText() {
        return inner.getText();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return inner.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return inner.findElement(by);
    }

    @Override
    public boolean isDisplayed() {
        return inner.isDisplayed();
    }

    @Override
    public Point getLocation() {
        return inner.getLocation();
    }

    @Override
    public Dimension getSize() {
        return inner.getSize();
    }

    @Override
    public Rectangle getRect() {
        return inner.getRect();
    }

    @Override
    public String getCssValue(String s) {
        return inner.getCssValue(s);
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return inner.getScreenshotAs(outputType);
    }
}
