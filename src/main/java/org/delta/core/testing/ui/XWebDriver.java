package org.delta.core.testing.ui;

import org.openqa.selenium.*;

import java.util.List;
import java.util.Set;

/**
 * This is a wrapper abstract class for configurable remote webdrivers.
 */
public abstract class XWebDriver implements WebDriver, TakesScreenshot{

    public final WebDriver driver ;

    public XWebDriver(WebDriver webDriver){
        driver = webDriver ;
    }
    @Override
    public void get(String s) {
        driver.get(s);
    }

    @Override
    public String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    @Override
    public String getTitle() {
        return driver.getTitle();
    }

    @Override
    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    @Override
    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    @Override
    public String getPageSource() {
        return driver.getPageSource();
    }

    @Override
    public void close() {
        driver.close();
    }

    @Override
    public void quit() {
        driver.quit();
    }

    @Override
    public Set<String> getWindowHandles() {
        return driver.getWindowHandles();
    }

    @Override
    public String getWindowHandle() {
        return driver.getWindowHandle();
    }

    @Override
    public TargetLocator switchTo() {
        return driver.switchTo();
    }

    @Override
    public Navigation navigate() {
        return driver.navigate();
    }

    @Override
    public Options manage() {
        return driver.manage();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        if ( driver instanceof TakesScreenshot ){
            return ((TakesScreenshot)driver).getScreenshotAs(outputType);
        }
        throw new WebDriverException("Taking Screenshot is not supported by the underlying driver!");
    }
}
