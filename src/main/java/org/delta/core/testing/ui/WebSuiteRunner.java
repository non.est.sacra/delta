package org.delta.core.testing.ui;

import org.delta.core.testing.TestSuite;
import org.delta.core.testing.TestSuiteRunner;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.io.File;
import java.util.Collections;
import java.util.Map;

/**
 */
public class WebSuiteRunner extends TestSuiteRunner {

    public final WebTestSuite webTestSuite ;

    public XSelenium xSelenium ;

    protected ZScript before;

    protected ZScript script ;

    protected ZScript after;

    protected ZContext.FunctionContext getContext(){
        ZContext.FunctionContext context = new ZContext.FunctionContext(ZContext.EMPTY_CONTEXT,
                ZContext.ArgContext.EMPTY_ARGS_CONTEXT );
        context.set(XSelenium.SELENIUM_VAR, xSelenium);
        return context;
    }

    public WebSuiteRunner(String file, Map<String,String> variables) throws Exception {
        super(variables);
        webTestSuite = WebTestSuite.loadFrom(file,variables);
    }

    public WebSuiteRunner(String file) throws Exception {
        this(file, Collections.EMPTY_MAP);
    }


    /**
     * Sets up variable for local context
     * @param local the local context
     * @param runEvent run event, which encapsulates the data source
     */
    public void setLocalContext(ZContext local, TestRunEvent runEvent){
        String[] columns = runEvent.table.row(0);
        String[] values = runEvent.table.row(runEvent.row);
        // put into context
        for ( int i = 0 ; i < columns.length;i++ ){
            local.set(columns[i],values[i]);
        }
    }

    @Override
    protected TestSuite testSuite() {
        return webTestSuite;
    }

    @Override
    protected void prepare() throws Exception {

        if (webTestSuite.remoteConfig.isEmpty() ){
            xSelenium = XSelenium.selenium(webTestSuite.webApp.url, webTestSuite.browserType );
        }else{
            xSelenium = XSelenium.selenium(webTestSuite.webApp.url, webTestSuite.remoteConfig );
        }
    }

    @Override
    protected TestSuite.Application application() {
        return webTestSuite.webApp ;
    }

    @Override
    protected String logLocation(String base, TestSuite.Feature feature) {
        String loc = webTestSuite.webApp.logs + "/" + base +"/" + feature.name ;
        File file = new File(loc);
        if ( !file.exists() ){
            file.mkdirs();
        }
        if ( xSelenium != null ) {
            xSelenium.screenShotDir(loc);
        }
        return loc;
    }

    @Override
    protected void afterFeature(TestSuite.Feature feature) throws Exception {
        before = null;
        script = null;
        after = null;
    }

    protected void createScripts( TestSuite.Feature feature) throws Exception{
        if ( !feature.script.isEmpty() ) {
            String scriptLocation = webTestSuite.webApp.scriptDir + "/" + feature.script;
            script = new ZScript(scriptLocation,null);
        }if ( !feature.beforeScript.isEmpty() ) {
            String scriptLocation = webTestSuite.webApp.scriptDir + "/" + feature.beforeScript;
            before = new ZScript(scriptLocation,null);
        }if ( !feature.afterScript.isEmpty() ) {
            String scriptLocation = webTestSuite.webApp.scriptDir + "/" + feature.afterScript;
            after = new ZScript(scriptLocation,null);
        }
    }

    @Override
    protected void beforeFeature(TestSuite.Feature feature) throws Exception {

        createScripts(feature);
    }

    @Override
    protected TestRunEvent runTest(TestRunEvent runEvent) throws Exception {
        // Get a copied context
        ZContext local = getContext();
        setLocalContext(local,runEvent);
        script.runContext(local);
        // set output if need be?
        Function.MonadicContainer mc = script.execute();
        if ( mc.value() instanceof Throwable ){
            runEvent.type = TestRunEventType.ERROR_TEST;
            runEvent.error = (Throwable) mc.value();
        } else {
            runEvent.type = TestRunEventType.OK_TEST;
            runEvent.runObject = mc.value();
        }
        return runEvent ;
    }

    @Override
    protected void shutdown() throws Exception {
        xSelenium.close();
        xSelenium = null;
    }
}
