
package org.delta.core.testing.ui;

import org.delta.core.testing.TestSuite;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import java.util.Map;

/**
 */

@XStreamAlias("testSuite")
public class WebTestSuite extends TestSuite {

    @XStreamAlias("browser")
    @XStreamAsAttribute
    public XSelenium.BrowserType browserType;

    @XStreamAsAttribute
    public String remoteConfig;

    @XStreamAlias("webApp")
    public static class WebApplication extends Application{

        @XStreamAsAttribute
        public String url;

        @XStreamAsAttribute
        public String method;

        @XStreamAsAttribute
        public int connectionTimeout;

        public WebApplication(){
            url = "" ;
            method = "" ;
            connectionTimeout = 10000;
        }
    }

    public WebApplication webApp;

    public WebTestSuite(){
        browserType = XSelenium.BrowserType.FIREFOX ;
        remoteConfig = "" ;
        webApp = new WebApplication();
    }

    public static WebTestSuite loadFrom(String file, Map<String,String> variables) throws Exception{
        return loadFrom(WebTestSuite.class , file, variables);
    }

}
