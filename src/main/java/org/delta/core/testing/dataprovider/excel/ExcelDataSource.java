
package org.delta.core.testing.dataprovider.excel;

import org.delta.core.testing.dataprovider.DataSource;
import org.delta.core.testing.dataprovider.DataSourceTable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Excel Files as data sources,
 * While use sheets as tables
 */
public class ExcelDataSource extends DataSource {

    public static final Pattern LOADER_PATTERN = Pattern.compile(".+\\.xls[x]?[m]?$",Pattern.CASE_INSENSITIVE);

    @Override
    public Pattern loadPattern() {
        return LOADER_PATTERN;
    }

    /**
     * Excel sheet as data table
     */
    public static class ExcelDataTable extends DataSourceTable {

        DataSource dataSource;

        String name;

        ArrayList<String[]> data;

        @Override
        public String name() {
            return name;
        }

        @Override
        public int length() {
            return data.size();
        }

        @Override
        public String[] row(int rowIndex) {
            if ( rowIndex < data.size() ) {
                return data.get(rowIndex);
            }
            return null;
        }

        @Override
        public DataSource dataSource() {
            return dataSource;
        }

        private void init(ExcelReader reader){
            data  = new ArrayList<>();
            int rowSize = reader.rowCount(name);
            for ( int row = 0 ; row < rowSize; row++ ){
                int colSize = reader.columnCount(name,row);
                String[] words = new String[colSize];
                for ( int col = 0 ; col < colSize; col++ ){
                    words[col] = reader.value(name, row, col);
                }
                data.add(words);
            }
        }

        /**
         * Creates an data table
         * @param reader excel reader
         * @param ds the parent data source object
         * @param sheetName the sheet to be used as table
         */
        public ExcelDataTable(ExcelReader reader, ExcelDataSource ds, String sheetName){
            name = sheetName ;
            dataSource = ds ;
            init(reader);
        }

    }

    @Override
    protected Map<String, DataSourceTable> init(String location) throws Exception {
        HashMap<String, DataSourceTable> tables = new HashMap<>();
        String l = location.toLowerCase();
        ExcelReader reader ;
        if ( l.endsWith(".xls")){
            reader = new XlsReader(location);
        }
        else{
            reader = new XlsXReader(location);
        }
        String[] sheetNames = reader.sheets();
        for ( int i = 0 ; i < sheetNames.length;i++ ){
            DataSourceTable table = new ExcelDataTable(reader, this, sheetNames[i]);
            tables.put(table.name(), table);
        }
        return tables;
    }

    public ExcelDataSource(String location) throws Exception {
        super(location);
    }
    public ExcelDataSource(){}
}
