
package org.delta.core.testing.dataprovider.excel;

import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Implementation of {ExcelReader} for .xlsx files
 */

public class XlsXReader implements ExcelReader {

    XSSFWorkbook workbook;

    String[] sheetNames;

    final FormulaEvaluator evaluator ;

    final DateFormat isoLike = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    /**
     * Conversion of arbitrary value types to string
     * Also reads formula cells
     * @param cell the cell to read from
     * @return the value as string
     */
    public  String readCellValueAsString(XSSFCell cell) {
        CellValue cellValue = evaluator.evaluate(cell);
        if ( cellValue == null ) return null;
        switch (cellValue.getCellType()) {
            case BLANK:
                return "";
            case BOOLEAN:
                return Boolean.toString(cell.getBooleanCellValue());
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    return isoLike.format(cell.getDateCellValue());
                }
                // else ....
                Double d = cell.getNumericCellValue();
                long l = d.longValue();
                Double compD = (double) l;
                if (compD.equals(d)) {
                    return Long.toString(l);
                } else {
                    return Double.toString(d);
                }
        }
        return null;
    }

    private void init(){
        ArrayList<String> names = new ArrayList<>();
        int numSheets = workbook.getNumberOfSheets();
        for ( int i = 0 ; i < numSheets; i++ ){
            names.add(  workbook.getSheetName(i) );
        }
        sheetNames = new String[names.size()];
        sheetNames = names.toArray( sheetNames );
    }

    /**
     * Creates an instance of the ExcelReaderXLSX
     *
     * @param fileName the excel xsl file to read from
     */
    public XlsXReader(String fileName) {
        try {
            workbook = new XSSFWorkbook(new FileInputStream(fileName));
            evaluator = workbook.getCreationHelper().createFormulaEvaluator();
            init();

        } catch (Exception e) {
            throw new Error(e);
        }
    }


    @Override
    public String[] sheets() {
        return sheetNames ;
    }

    @Override
    public String value(String sheet, int row, int column) {
        XSSFCell cell = workbook.getSheet(sheet).getRow(row).getCell(column);
        return readCellValueAsString(cell);
    }

    @Override
    public int rowCount(String sheet) {
        return workbook.getSheet(sheet).getLastRowNum() + 1 ;
    }

    @Override
    public int columnCount(String sheet) {
        return workbook.getSheet(sheet).getRow(0).getLastCellNum() ;
    }

    @Override
    public int columnCount(String sheet, int rowNum){
        return workbook.getSheet(sheet).getRow(rowNum).getPhysicalNumberOfCells();
    }
}