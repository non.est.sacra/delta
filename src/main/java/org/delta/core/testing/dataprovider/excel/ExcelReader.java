
package org.delta.core.testing.dataprovider.excel;

/**
 * Interface ExcelReader to make it easy to read excel
 */
public interface ExcelReader {

    /**
     * gets the names of the sheets of the  excel file
     * @return String[] of the names of the worksheets
     */
    String[] sheets();

    /**
     * Gets the value of the excel cell as string
     * @param sheet the sheet name
     * @param row  0 based row index
     * @param column  column name
     * @return value of the cell, casted to string
     */
    String value(String sheet, int row, int column);

    /**
     * Gets the row count in the sheetName
     * @param sheet sheet name
     * @return the number of rows in the sheet
     */
    int rowCount(String sheet);

    /**
     * Gets the column  count in the sheet for the row
     * @param sheet sheet name
     * @return the column count
     */
    int columnCount(String sheet);

    /**
     * This is for non matrix excel
     * @param sheet name of
     * @param rowNum index of, 0 based
     * @return the column count ( no of cells )
     */
    default int columnCount(String sheet, int rowNum){
        return columnCount(sheet);
    }
}
