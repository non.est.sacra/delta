package org.delta.core.testing.dataprovider;

import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZTypes;
import java.util.Map;

/**
 * A Data Table type to ease out data reading
 */
public abstract class DataSource implements ZMatrix.ZMatrixLoader {

    protected String loc;

    public String location(){ return  loc ; }

    public Map<String,DataSourceTable> tables;

    protected abstract Map<String,DataSourceTable> init(String location) throws Exception;

    @Override
    public ZMatrix load(String s, Object... objects) {
        DataSource dataSource = ProviderFactory.dataSource(s);
        if ( objects.length ==  0 ){
            throw new UnsupportedOperationException("Sorry, no sheet was specified!");
        }
        String sheet = objects[0].toString();
        DataSourceTable dataSourceTable = dataSource.tables.get(sheet);
        if ( dataSourceTable == null ){
            throw new UnsupportedOperationException("Sorry, no  such sheet as '" + sheet +"'!");
        }
        boolean header = true ;
        if ( objects.length >  1 ){
            header = ZTypes.bool(objects[1],false );
        }
        return dataSourceTable.matrix(header);
    }

    public DataSource(String location) throws Exception{
        this.loc = location;
        this.tables = init(location);
    }

    public DataSource(){
        this.loc = null;
        this.tables = null;
    }
}

