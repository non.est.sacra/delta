package org.delta.core.testing.dataprovider;

import org.openqa.selenium.InvalidArgumentException;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.collections.ZMap;
import zoomba.lang.core.operations.ZMatrix;
import zoomba.lang.core.types.ZNumber;

import java.util.*;
import java.util.stream.Collectors;

/**
 * A DataSourceTable type for Table like manipulation of data
 */

public abstract class DataSourceTable {

    public final class InMemoryMatrix extends ZMatrix.BaseZMatrix{

        public final DataSourceTable table;

        public final boolean header;

        public InMemoryMatrix(DataSourceTable table, boolean header){
            this.table = table;
            this.header = header ;
        }

        @Override
        public List row(int i) {
            return new ZArray( table.row( i),false );
        }

        @Override
        public int rows() {
            return table.length();
        }
    }


    protected HashMap<String,Integer> columns;

    /**
     * Gets the columns
     * @return a Map : column name with index
     */
    public Map<String,Integer> columns(){
        if ( columns == null ){
            populateColumns();
        }
        return columns ;
    }

    /**
     * Gets the name of the data source
     * @return the name
     */
    public abstract String name();

    /**
     * The size of the data source
     * @return number of rows
     */
    public abstract int length();

    /**
     * Gets a row
     * @param rowIndex the index
     * @return a string array - corresponding to the row values
     */
    public abstract String[] row(int rowIndex);


    public abstract DataSource dataSource();

    private void populateColumns(){
        columns = new HashMap<>();
        String[] cols = row(0);
        for ( int i = 0 ; i < cols.length;i++ ){
            columns.put(cols[i].trim(), i);
        }
    }

    /**
     * Gets the columns value
     * @param column name of the column ( header )
     * @param rowIndex the row index
     * @return column value
     */
    public String columnValue(String column, int rowIndex){
        if ( columns == null ){
            populateColumns();

        }
        if ( !columns.containsKey(column) ) {
            return null;
        }
        String[] data = row( rowIndex ) ;
        return data[columns.get(column)] ;
    }

    /**
     * A Tuple of column name to value for a row
     * @param rowIndex the row index
     * @return a map header : column value mapping
     */
    public Map<String,String> tuple(int rowIndex){
        String[] columns = row(0);
        String[] values = row(rowIndex);
        try {
            return new ZMap(columns, values);
        }
        catch (Exception e){
            return Collections.EMPTY_MAP ;
        }
    }


    protected Object object( String v){
        boolean isMap = v.startsWith("%");
        boolean isList = v.startsWith("@");
        if ( !isMap && !isList ) return v;
        String r = v.substring(1);
        String[] arr = r.split(":");
        final String tableName = arr[0].trim();
        final int rowInx = ZNumber.integer(arr[1],0).intValue();
        DataSourceTable table = dataSource().tables.get(tableName);
        if ( table == null ){
            throw new InvalidArgumentException("Table name does not exists : " + tableName);
        }
        String[] others = table.row(rowInx);
        if ( isList){ // list
            final List<Object> l = Arrays.stream(others).map( this::object ).collect(Collectors.toList());
            return l;
        }
        final Map<String,Object> m = Arrays.stream( others ).map( x-> x.split("#")) .collect(
                Collectors.toMap(a -> a[0], a->  object( a[1] ) ));
        return m;
    }

    /**
     * A Tuple of column name to value for a row, value can actually be a String, map, or an Array
     * @param rowIndex the row index
     * @return a map header : column value mapping
     */
    public Map<String,Object> relationalTuple(int rowIndex){
        String[] columns = row(0);
        String[] values = row(rowIndex);
        Map<String,Object> res = new LinkedHashMap<>();
        for ( int i =0; i < columns.length; i++ ){
            String v = values[i];
            Object o = object(v);
            res.put(columns[i],o);
        }
        return res;
    }

    /**
     * Creates a read/write data matrix from the underlying data
     * Note that changes won't be saved back to the data source
     * @param header if true, header info is passed to matrix to do column wise manipulation
     *               if false, header is taken as data
     * @return a data matrix
     */
    public ZMatrix matrix(boolean header){
        return new InMemoryMatrix(this,header) ;
    }
}