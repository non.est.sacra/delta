package org.delta.core.testing.dataprovider;

import org.delta.core.testing.Utils;
import org.delta.core.testing.dataprovider.dir.DirectoryDataSource;
import org.delta.core.testing.dataprovider.excel.ExcelDataSource;
import org.delta.core.testing.dataprovider.uri.URIDataSource;
import zoomba.lang.core.operations.ZMatrix;

import java.util.HashMap;
import java.util.regex.Pattern;

/**
 */
public final class ProviderFactory {

    public static final HashMap<Pattern,String> dataSources = new HashMap<>();

    static{

        dataSources.put(DirectoryDataSource.LOADER_PATTERN,DirectoryDataSource.class.getName());
        ZMatrix.Loader.add(  new DirectoryDataSource() );

        dataSources.put(ExcelDataSource.LOADER_PATTERN,ExcelDataSource.class.getName());
        ZMatrix.Loader.add(  new ExcelDataSource() );

        dataSources.put(URIDataSource.LOADER_PATTERN, URIDataSource.class.getName());
        ZMatrix.Loader.add(  new URIDataSource() );

    }

    // public so that anyone can clear the cache if need be
    public static final HashMap<String,DataSource> caches = new HashMap<>();

    public static DataSource  dataSource(String location){
        if ( caches.containsKey(location) ){
            return caches.get(location);
        }
        for ( Pattern key : dataSources.keySet() ){
            if ( key.matcher(location).matches() ){
                String className = dataSources.get(key);
                try {
                    Object ds = Utils.createInstance(className, location);
                    caches.put(location,(DataSource)ds);
                    return (DataSource) ds;
                }catch (Exception e){
                    System.err.println("Error creating :" + className );
                    System.err.println(e);
                }
            }
        }
        return null;
    }
}
