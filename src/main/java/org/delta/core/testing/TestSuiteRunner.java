package org.delta.core.testing;

import org.delta.core.testing.dataprovider.DataSource;
import org.delta.core.testing.dataprovider.DataSourceTable;
import org.delta.core.testing.dataprovider.ProviderFactory;
import org.delta.core.testing.reporting.Reporter;
import zoomba.lang.core.collections.ZList;
import zoomba.lang.core.operations.ZRandom;
import zoomba.lang.core.types.ZRange;
import zoomba.lang.core.types.ZTypes;

import java.util.*;

/**
 */
public abstract class TestSuiteRunner implements Runnable{

    public static final String SCRIPT_OUT = "_o_" ;

    public enum TestRunEventType{
        PREPARE,
        BEFORE_FEATURE,
        BEFORE_TEST,
        OK_TEST,
        ERROR_TEST,
        ABORT_TEST,
        IGNORE_TEST,
        AFTER_FEATURE
    }

    public static class TestRunEvent extends EventObject{

        public TestRunEventType type ;

        public final String feature ;

        public final DataSourceTable table;

        public final int row;

        public Object runObject;

        public Throwable error;

        public TestRunEvent(Object source,TestRunEventType type, String feature, DataSourceTable table ,int row) {
            super(source);
            this.type = type ;
            this.feature = feature;
            this.row = row;
            this.table = table;
        }
    }

    public interface TestRunEventListener{

        void onTestRunEvent(TestRunEvent testRunEvent);

    }

    public final Set<TestRunEventListener> testRunEventListeners ;

    protected Set<Reporter> reporters;

    public static class DataSourceContainer{

        public final TestSuite.DataSource suiteDataSource;

        public final DataSource dataSource;

        public DataSourceContainer(TestSuite.DataSource ds )throws Exception{
            this.suiteDataSource = ds ;
            dataSource = ProviderFactory.dataSource(suiteDataSource.location);
            if ( dataSource == null ){
                throw new Exception("Can not create data source!");
            }
        }
    }

    protected Map<String,DataSourceContainer> dataSources;

    protected Map<String,TestSuite.DataSource> tsDataSources;

    protected Map<String,String> relocationVariables;

    protected TestSuiteRunner(Map<String,String> v){
        testRunEventListeners = new HashSet<>();
        relocationVariables = v;
    }


    protected void fireTestEvent(String feature, TestRunEventType type, DataSourceTable table, int row){
        TestRunEvent event = new TestRunEvent( this, type, feature, table, row);
        fireTestEvent(event);
    }

    protected void fireTestEvent(TestRunEvent event){

        switch (event.type ){
            case ABORT_TEST:
                aborts.add( event );
                break;
            case ERROR_TEST:
                errors.add(event);
                break;
            default:
                break;
        }

        for (TestRunEventListener listener : testRunEventListeners ){
            try {
                listener.onTestRunEvent( event );
            }catch (Throwable t){
                System.err.println("Error dispatching event : " + t);
            }
        }
    }

    protected abstract TestSuite testSuite();

    protected abstract TestSuite.Application application();

    protected abstract void prepare() throws Exception;

    protected abstract void beforeFeature(TestSuite.Feature feature) throws Exception;

    protected abstract String logLocation(String base, TestSuite.Feature feature) ;

    protected abstract  TestRunEvent runTest( TestRunEvent runEvent) throws Exception;

    protected abstract void afterFeature(TestSuite.Feature feature) throws Exception;

    protected abstract void shutdown() throws Exception;

    protected void prepareDSAndReporters() throws Exception {
        dataSources = new HashMap<>();
        tsDataSources = new HashMap<>();
        for (TestSuite.DataSource ds : testSuite.dataSources ){
            DataSourceContainer container = new DataSourceContainer(ds);
            dataSources.put( ds.name, container );
        }
        reporters = new HashSet<>();
        for (TestSuite.Reporter r : testSuite.reporters ){
            Reporter reporter = (Reporter) Utils.createInstance(r.type);
            reporter.init( r.params );
            reporters.add(reporter);
        }
        testRunEventListeners.addAll(reporters);
        aborts = new ArrayList<>();
        errors = new ArrayList<>();
    }

    protected void changeLogDirectory(TestSuite.Feature feature){
        String timeStamp = Utils.ts();
        String logDir = logLocation(timeStamp, feature);
        for ( Reporter r : reporters ){
            r.location(logDir);
        }
    }

    protected DataSourceTable dataSourceTable(TestSuite.Feature feature) {
        DataSourceContainer container = dataSources.get(feature.ds) ;
        if ( container == null ){
            System.err.printf("No Such data source : [%s]\n",feature.ds);
            return null;
        }
        DataSourceTable table = container.dataSource.tables.get(feature.table);
        if ( table == null ){
            System.err.printf("No Such data table in Data Source : [%s] [%s]\n",feature.table, feature.ds);
        }
        return table;
    }

    boolean skipTest(TestSuite.Feature feature, int row){
        DataSourceContainer container = dataSources.get(feature.ds) ;
        DataSourceTable t = container.dataSource.tables.get(feature.table);
        boolean exist = t.columns().containsKey( container.suiteDataSource.testEnableColumn );
        if ( !exist ){
            return false ;
        }
        String value = t.columnValue(container.suiteDataSource.testEnableColumn, row) ;
        boolean enable = ZTypes.bool(value, false);
        return !enable;
    }

    protected TestSuite testSuite ;

    protected Collection<TestRunEvent> aborts;

    public Collection<TestRunEvent> aborts(){ return aborts; }

    protected Collection<TestRunEvent> errors;

    public Collection<TestRunEvent> errors(){ return errors; }

    @Override
    public void run() {
        // get it first...
        testSuite = testSuite();
        try{
            prepareDSAndReporters();
            prepare();
        }catch (Exception e) {
            System.err.printf("Failed to prepare test Suite! %s\n", e);
            TestRunEvent fatal = new TestRunEvent(this, TestRunEventType.PREPARE, "*", null, -1);
            fatal.error = e;
            this.aborts.add(fatal);
             return;
         }

        TestSuite.Application application = application();
        for ( int i = 0 ; i < application.features.size() ;i++){

            TestSuite.Feature feature = application.features.get(i);
            if ( !feature.enabled ){
                continue;
            }
            try{
                beforeFeature(feature);
            }catch (Exception e){
                System.err.printf("Error : %s\n Skipping Feature %s\n", e , feature.name );
                continue;
            }
            changeLogDirectory(feature);

            fireTestEvent(feature.name,TestRunEventType.BEFORE_FEATURE, null, -1);

            DataSourceTable table = dataSourceTable(feature);
            if ( table == null ){
                System.err.println("Sorry, can not create data source!");
                fireTestEvent(feature.name,TestRunEventType.AFTER_FEATURE, null, -1);
                return;
            }
            ZRange.NumRange iterator = new ZRange.NumRange( 1, table.length());
            List<Number> rows = new ZList( iterator.asList() );
            if ( feature.randomize ){
                ZRandom.shuffle(rows);
            }

            for ( Number numRow : rows ){
                int row = numRow.intValue();
                boolean skip = skipTest(feature,row) ;
                if ( skip ){
                    fireTestEvent(feature.name, TestRunEventType.IGNORE_TEST, table, row);
                    continue;
                }
                fireTestEvent(feature.name, TestRunEventType.BEFORE_TEST, table, row);
                TestRunEvent runEvent = new TestRunEvent(this, TestRunEventType.ERROR_TEST, feature.name, table, row) ;
                try {
                    runEvent = runTest(runEvent);
                }catch (Throwable t){
                    System.err.println(t);
                    runEvent.type = TestRunEventType.ERROR_TEST;
                    runEvent.error = t ;
                }
                fireTestEvent(runEvent);
            }
            fireTestEvent(feature.name,TestRunEventType.AFTER_FEATURE, null, -1);
            try{
                afterFeature(feature);
            }catch (Exception e){
                continue;
            }
        }
        try{
            testRunEventListeners.clear();
            shutdown();
        }catch (Exception e){
            System.err.println(e);
            return;
        }
    }
}
