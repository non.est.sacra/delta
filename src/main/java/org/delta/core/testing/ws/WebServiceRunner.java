package org.delta.core.testing.ws;

import org.delta.core.testing.TestSuite;
import org.delta.core.testing.Utils;
import org.delta.core.testing.ui.WebSuiteRunner;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.types.ZTypes;

import java.util.Collections;
import java.util.Map;

/**
 */
public class WebServiceRunner extends WebSuiteRunner {

    final static Utils.Logger logger = new Utils.Logger(WebServiceRunner.class);

    protected RestCaller restCaller;

    protected void createCaller(TestSuite.Feature feature) throws Exception{
        String callUrl = webTestSuite.webApp.url;
        if (!feature.base.isEmpty()) {
            callUrl += "/" + feature.base + "/";
        }
        String method = webTestSuite.webApp.method;
        if (!feature.method.isEmpty()) {
            method = feature.method;
        }
        restCaller = new RestCaller(callUrl, method,
                webTestSuite.webApp.connectionTimeout, feature.timeout );

    }

    @Override
    protected void beforeFeature(TestSuite.Feature feature) throws Exception {
        createCaller(feature);
        super.beforeFeature(feature);
    }

    @Override
    protected void afterFeature(TestSuite.Feature feature) throws Exception {
        restCaller = null;
        super.afterFeature(feature);
    }

    public WebServiceRunner(String file, Map<String,String> variables) throws Exception {
        super(file,variables);
        restCaller = null;
    }

    public WebServiceRunner(String file) throws Exception {
        this(file, Collections.EMPTY_MAP);
    }

    @Override
    protected void prepare() throws Exception {

    }

    @Override
    protected TestRunEvent runTest(TestRunEvent runEvent) throws Exception {

        ZContext local = new ZContext.FunctionContext(ZContext.EMPTY_CONTEXT,
                ZContext.ArgContext.EMPTY_ARGS_CONTEXT ) ;
        setLocalContext(local,runEvent);
        boolean run = true;
        //run before test
        if (before != null) {
            try {
                Object ret = before.execute(local);
                run = ZTypes.bool(ret, false);
            } catch (Exception e) {
                //ignore now , just disable test
                run = false;
                logger.error("Pre validator encountered error", e);
                runEvent.error = e;
            }
        }
        if (!run) {
            runEvent.type = TestRunEventType.ABORT_TEST;
            return runEvent;
        }
        Object result = null;
        try {
            Map args = runEvent.table.tuple( runEvent.row );
            result = restCaller.call(args);
        } catch (Throwable t) {
            run = false;
            runEvent.error = t;
            logger.error("Web Service Call encountered error", t);
        }
        // any assertions failed?
        if ( run ){
            //run = !testAssert.hasError();
        }

        if (!run) {
            runEvent.type = TestRunEventType.ERROR_TEST;
            return runEvent;
        }

        if (after != null) {
            //set up nicely
            local.set(SCRIPT_OUT, result);

            try {
                Object ret = after.execute(local);
                run = ZTypes.bool(ret, false);
            } catch (Exception e) {
                runEvent.error = e;
                logger.error("Post Validator encountered error", e);
            }
        }
        if (run) {
            runEvent.type = TestRunEventType.OK_TEST;
            runEvent.runObject = result;
        } else {
            runEvent.type = TestRunEventType.ERROR_TEST;
        }

        return runEvent;
    }

    @Override
    protected void shutdown() throws Exception {
        // do nothing
    }
}
