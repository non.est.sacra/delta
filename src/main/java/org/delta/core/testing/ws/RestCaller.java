package org.delta.core.testing.ws;

import zoomba.lang.core.io.ZFileSystem;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

/**
 */
public class RestCaller {

    public enum CallType{
        GET,
        POST
    }

    public static final String ENCODING = "UTF-8";

    protected URL base;

    //disabling switching... probably makes sense this way?
    public final CallType method;

    protected int connectionTimeout;

    protected int readTimeout;


    public RestCaller(String url, String method, int connectionTimeout, int readTimeOut) throws Exception{
        base = new URL(url) ;
        this.method = Enum.valueOf( CallType.class, method);
        this.connectionTimeout = connectionTimeout ;
        this.readTimeout = readTimeOut ;
    }

    public String createRequest(Map<String,String> args) throws Exception{
        if (args == null || args.isEmpty()){
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        for ( String name : args.keySet()){
            buffer.append(name).append("=");
            String value = args.get(name);
            value = URLEncoder.encode( value, ENCODING) ;
            buffer.append(value).append("&");
        }
        String query = buffer.substring(0, buffer.length() - 1); // remove last
        return query ;
    }

    public String get(Map<String,String> args) throws Exception {
        String finalUrl = base.toString() + "?" + createRequest(args);
        String result = (String)ZFileSystem.read(finalUrl, connectionTimeout, readTimeout );
        return result;
    }

    /**
     * http://stackoverflow.com/questions/2793150/using-java-net-urlconnection-to-fire-and-handle-http-requests
     * @param args the arguments to pass
     * @return the response
     * @throws Exception in case of any issue
     */
    public String post(Map<String,String> args) throws Exception {
        URLConnection  connection =  base.openConnection();
        connection.setDoOutput(true); // make it post
        connection.setRequestProperty("Accept-Charset", ENCODING);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + ENCODING);
        connection.setConnectTimeout( connectionTimeout );
        connection.setReadTimeout( readTimeout );
        String query = createRequest(args);
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write( query.getBytes(ENCODING) );
        InputStream inputStream = connection.getInputStream();
        return "" ; // later fix...
    }

    public String call(Map<String,String> args) throws Exception {
        switch ( method ){
            case GET:
                return get(args);
            case POST:
                return post(args);
            default:
                return "Unsupported Protocol!" ;
        }
    }
}
