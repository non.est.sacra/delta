package org.delta.core.testing.api;

import org.delta.core.testing.api.junit.JClassRunner;
import org.junit.runner.RunWith;
import static org.delta.core.testing.api.Annotations.* ;
import java.security.SecureRandom;

/**
 */

@RunWith(JClassRunner.class)
@NApiService(base = "samples/")
@NApiServiceCreator
public class NApiAnnotationSample {

    static final boolean _RANDOM_EX_ = false ;

    @NApiServiceInit
    public NApiAnnotationSample(){}

    @NApi(use=false, dataSource = "UIData.xlsx", dataTable = "add" ,
            before = "pre.zm", after = "post.zm", globals = {"op=+"} )
    @NApiThread(use = false)
    public int add(int a, int b) {
        int r = a + b ;
        System.out.printf("%d + %d = %d \n", a, b, r );
        return r;
    }

    @NApi(randomize = true, dataSource = "UIData.xlsx", dataTable = "sub" ,
            before = "pre.zm", after = "post.zm" , globals = { "op=-" } )
    @NApiThread(dcd = true, performance = @Performance())
    public int subtract(int a, int b) {
        int r = a - b ;
        System.out.printf("%d - %d = %d \n", a, b, r );
        SecureRandom sr = new SecureRandom();
        if ( _RANDOM_EX_ && sr.nextBoolean()){ throw new Error("Random Error!"); }
        return r;
    }
}
