package org.delta.core.testing;

import org.delta.core.testing.dataprovider.ProviderFactory;
import org.delta.core.testing.reporting.SimpleTextReporter;
import org.delta.core.testing.ui.WebSuiteRunner;
import org.delta.core.testing.ui.XSelenium;
import org.delta.core.testing.ws.WebServiceRunner;
import org.jsoup.Jsoup;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;
import zoomba.lang.core.types.ZException;
import static org.kohsuke.args4j.ExampleMode.ALL;

import java.util.*;

/**
 * A Main class to have command line interface, if need be.
 */
public class Main {

    @Option(name = "-h", usage = "Show this help, and exits")
    boolean __HELP__ = false;

    @Option(name = "-X", usage = "run with full debug information")
    boolean __DEBUG__ = false;

    @Option(name = "-u", usage = "url to run script against")
    private String url = "";

    @Option(name = "-s", usage = "Test Suite Xml file")
    private String suite = "";

    @Option(name = "-b", usage = "Local Browser Type to Use")
    private XSelenium.BrowserType browserType = XSelenium.BrowserType.FIREFOX;

    @Option(name = "-bc", usage = "Remote Browser Configuration to Use, if no local browser")
    private String remoteBrowserConfiguration = "";

    // receives other command line parameters than options
    @Argument
    private List<String> arguments = new ArrayList<>();

    /**
     * In case of normal script w/o url
     * Executes the script
     */
    public void executeScript() {
        String[] args = new String[arguments.size()];
        args = arguments.toArray(args);
        if (args.length == 0) {
            try {
                System.out.println("Entering REPL Mode...for Zoomba");
                zoomba.lang.Main.main( args );
                System.exit(0);
            } catch (Throwable t) {
                System.exit(1);
            }
        }

        ZContext.FunctionContext context =
                new ZContext.FunctionContext(ZContext.EMPTY_CONTEXT,
                        ZContext.ArgContext.EMPTY_ARGS_CONTEXT);
        // should also put JSoup...
        context.set("jsoup", Jsoup.class);
        try {
            ZScript sc = new ZScript(args[0],null);
            sc.runContext(context);
            Function.MonadicContainer mc = sc.execute();
            Object o = mc.value();
            if ( o instanceof Throwable ){
                throw (Throwable)o;
            }
            int e = 0;
            if (o instanceof Integer) {
                e = (int) o;
            }
            System.exit(e);
        } catch (Throwable e) {
            if (__DEBUG__) {
                System.err.println(e);
                e.printStackTrace();
            } else {
                if (e instanceof ZException) {
                    System.err.printf("Error : %s\n", e);
                    if ( e instanceof ZException.Variable && e.toString().contains("selenium") ){
                        System.err.println("Please run with selenium. See usage using '-h' flag.");
                    }
                } else {
                    System.err.println(e.getMessage());
                    if (e.getCause() != null) {
                        System.err.println(e.getCause().getMessage());
                    }
                }
            }
            System.exit(1);
        }
    }

    private void executeSeleniumScript() {

        if (arguments.isEmpty()) {
            System.err.println("No args given to run!");
            return;
        }

        String[] args = new String[arguments.size()];
        args = arguments.toArray(args);
        String file = arguments.get(0);

        ZContext.FunctionContext context =
                new ZContext.FunctionContext( ZContext.EMPTY_CONTEXT, new ZContext.ArgContext(args));
        XSelenium xSelenium;
        if (remoteBrowserConfiguration.isEmpty()) {
            xSelenium = XSelenium.selenium(url, browserType);
        } else {
            xSelenium = XSelenium.selenium(url, remoteBrowserConfiguration);
        }

        context.set(XSelenium.SELENIUM_VAR, xSelenium);
        context.set(XSelenium.BASE_URL, url);
        // should also put JSoup...
        context.set("jsoup", Jsoup.class);

        try {
            ZScript sc = new ZScript(file,null);
            sc.runContext(context);
            SimpleTextReporter textReporter = SimpleTextReporter.reporter(1, "");
            sc.assertion().eventListeners.add(xSelenium);
            sc.assertion().eventListeners.add(textReporter);

            Function.MonadicContainer mc = sc.execute();
            Object o = mc.value();
            if ( o instanceof Throwable){
                throw (Throwable)o;
            }
            int e = 0;
            if ( o instanceof Integer) {
                e = (int) o ;
            }
            xSelenium.close();
            System.exit(e);
        } catch (Throwable e) {
            if (__DEBUG__) {
                System.err.println(e);
                e.printStackTrace();
            } else {
                if (e instanceof ZException) {
                    System.err.printf("Error : %s\n", e);
                } else {
                    System.err.println(e.getMessage());
                    if (e.getCause() != null) {
                        System.err.println(e.getCause().getMessage());
                    }

                }
            }
            xSelenium.close();
            System.exit(1);
        }
    }

    public static TestSuiteRunner runner(String suiteFile) throws Exception {
        if (suiteFile.endsWith(".api.xml")) {
            return new WebServiceRunner(suiteFile);
        }
        return new WebSuiteRunner(suiteFile);
    }

    private void executeTestSuite(String suiteFile) {
        try {
            // get a runner...
            TestSuiteRunner runner = runner(suiteFile);
            // get on with the show...
            runner.run();
            System.exit(0);

        } catch (Throwable t) {
            if (__DEBUG__) {
                t.printStackTrace();
            } else {
                System.err.println(t);
                System.exit(1);
            }
        }
    }

    private void usage(CmdLineParser parser, boolean error) {
        if ( error ){
            System.err.println("Error in arguments, please see the options :");
        }
        System.err.println("java -jar <jar-file> [options...] arguments...");
        System.err.println("With no options or arguments starts a REPL Shell.");
        System.err.println("First Non Optional Argument is taken as a script file.");

        // print the list of available options
        parser.printUsage(System.err);
        System.err.println();

        // print option sample. This is useful some time
        System.err.println("  Example: java -jar <jar-file> " + parser.printExample(ALL));
        System.exit(-1);

    }

    private void run(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        parser.setUsageWidth(80);
        try {
            // parse the arguments.
            parser.parseArgument(args);
            if ( __HELP__ ){
                usage(parser,false);
                System.exit(0);
            }
        } catch (Exception e) {
            usage(parser,true);
            System.exit(1);
        }
        try {

            if (!suite.isEmpty()) {
                executeTestSuite(suite);
            }
            if (!url.isEmpty()) {
                executeSeleniumScript();
            }
            // go with free hand call : non selenium
            executeScript();
            System.exit(0);
        } catch (Throwable e) {
            // if there's a problem in the command line,
            // you'll get this exception. this will report
            // an error message.
            e.printStackTrace();
            System.exit(1);
        }
    }

    private Main() {
    }

    public static void main(String[] args) {
        // just load the class : ensures that matrix can now load stuff
        ProviderFactory.dataSource("");
        Main main = new Main();
        main.run(args);
    }
}
